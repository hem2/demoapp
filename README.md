
Navigation styles:

 <nav v-show="loginStatus">
                <ul class="menu color-gray-10 pull-left">
                    <li v-for="(link, index) in links" :key="index">
                        <router-link :to="link.path">{{ link.name }}</router-link>
                    </li>
                </ul>
<style lang="scss" scoped>
    .menu li {
        position: relative;
        display: inline-block;
        padding: 4px 8px;
        cursor: pointer;
        a {
            color: #fff;
            box-shadow: none;
        }
    }
</style>