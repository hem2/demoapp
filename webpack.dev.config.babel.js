/* global __dirname process require */
import path from 'path';
import webpack from 'webpack';

export default {
	entry: [
		'webpack-hot-middleware/client', path.join(__dirname, 'client/src/main.js')
	],

	output: {
		filename: 'bundle.js',
		path: path.join(__dirname, 'client/dist/assets'),
		publicPath: '/'
	},

	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new webpack.NoEmitOnErrorsPlugin(),
		new webpack.optimize.UglifyJsPlugin(),
		new webpack.LoaderOptionsPlugin({
			vue: {
				postcss: [
					require('autoprefixer')(),
				]
			}
		}),
	],

	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				options: {
					loaders: {
						js: 'babel-loader',
						scss: 'vue-style-loader!css-loader!sass-loader'
					}
				}
			},
			{
				test: /\.css$/,
				use: [
					'style-loader',
					'css-loader',
					'postcss-loader'
				]
			},
			{
				test: /\.scss$/,
				use: [
					'style-loader',
					'css-loader',
					'sass-loader',
					'postcss-loader'
				]
			},
			{
				test: /\.svg$/,
				loader: 'vue-svg-loader',
			},
			{
				test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
				loader: 'url-loader',
			},
		]
	},
	resolve: {
		extensions: ['.js', '.png', '.gif', '.vue'],
		alias: {
			vue: 'vue/dist/vue.js',
		}
	},
	resolveLoader: {
		alias: {
			'scss-loader': 'sass-loader',
		},
	},

	devtool: 'eval-source-map'
};
