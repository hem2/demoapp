import fs from 'fs';
import config from '../webpack.dev.config.babel';

class Configuration {

	static getUploadSizeLimit() {
		return process.env.FILE_SIZE_LIMIT || 20971520;
	}

	static getFormattedUploadSizeLimit() {
		return process.env.FORMATTED_FILE_SIZE_LIMIT || '20MB';
	}

	static getEnv() {
		return process.env.NODE_ENV;
	}

	static isDevelopment() {
		return ['production', 'test'].indexOf(this.getEnv()) === -1;
	}

	static getPort() {
		return process.env.PORT || 4003;
	}

	static getWebPackDevConf() {
		return config;
	}

	static getCookiesSecret() {
		return process.env.COOKIES_SECRET || 'c00k1es3cr3t';
	}
}

export default Configuration;