/* eslint-disable no-undef */
import path from 'path';
import express from 'express';
import compress from 'compression';
import winston from 'winston';
import bodyParser from 'body-parser';
import session from 'express-session';
import SessionStore from 'session-file-store';
import https from 'https';
import fs from 'fs';

import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';

import Config from './Configuration';

const dev  = Config.isDevelopment();
const port = Config.getPort();

const app = express();
const FileStore = new SessionStore(session);


app.enable('trust proxy');



app.use(bodyParser.json({ limit: Config.getFormattedUploadSizeLimit() }));
app.use(express.static(__dirname));
app.use(express.static(path.join(__dirname, '../client/src/assets')));
app.use(express.static(path.join(__dirname, '../node_modules/bigbank-interface-components/dist/'), { maxage: '12h' }));

app.use(session({
	store: new FileStore({
		logFn: () => {} // suppress useless logs
	}),
	secret: Config.getCookiesSecret(),
	resave: false,
	httpOnly: false,
	saveUninitialized: false
}));

/**
 * Development server supports modules hot reloading
 */
if (dev) {
	const config = Config.getWebPackDevConf();
	const compiler = webpack(config);

	app.use(webpackHotMiddleware(compiler));
	app.use(webpackDevMiddleware(compiler, {
		noInfo: true,
		publicPath: config.output.publicPath,
		stats: {
			colors: true,
		}
	}));
}

app.get('/', (req, res) => {
	res.sendFile(path.join(__dirname, '../client/dist/index.html'));
});

app.get('/status/technical-error', (req, res) => {
	res.status(500).sendFile(path.join(__dirname, '../client/dist/index.html'));
});

// Handle all other routes Vue-side
app.get('*', (req, res) => {
	res.sendFile(path.join(__dirname, '../client/dist/index.html'));
});

let server = app.listen(port, () => {
	winston.log('info', 'Application listening on port: ' + port);
	winston.log('info', 'Application mode: ' + (dev ? 'DEVELOPMENT' : 'PRODUCTION'));
});

export default server;