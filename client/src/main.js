import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import Components from 'bigbank-interface-components';
import store from './store/store'
import router from './router/router'

Vue.use(Components);

new Vue({
	router,
	store,
	el: 'app',
	components: {App}
});